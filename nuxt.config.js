import pkg from './package'

export default {
  mode: 'universal',

  head: {
    title: 'Ruby Floors – Parket, Laminat, Vinyl, grīdas segumi',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Parketa salons/veikals „RUBYFLOORS”. Jaunmoku iela 13, Rīga, T/C Spice Home 2. Stāvs'},
      { hid: 'keywords', name: 'keywords', content: 'parkets, franču skuja,	parkets atlaides, spice, spice home, gridas segumi, vinils,trīsslāņu parkets, lamināts, akcija, mākslinieciskāis parkets'}
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700'},
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Cinzel'}

    ]
  },

  loading: {
    color: 'white',
    height: '1px'
  },
  cache: true,

  plugins: [
    '~/plugins/i18n.js',
    { 
      src: '~/plugins/slick', 
      ssr: false 
    }
  ],

  modules: [
    'nuxt-svg-loader',
    ['@nuxtjs/google-tag-manager', { id: 'GTM-TZKXRD8' }],
    ['@nuxtjs/google-analytics', { id: 'UA-141774150-1'}],
    ['@nuxtjs/pwa', { icon: false }],
    '@nuxtjs/axios',
    'nuxt-payload-extractor'
  ],

  build: {
    vendor: ['vue-i18n'] 
  },
  router:{
    middleware: 'i18n',
    scrollBehavior (to, from, savedPosition) {
        return {x: 0, y: 0}
    }

  },


}
