import axios from 'axios'


export default{
    url: 'http://rubyfloors.com/admin/',
    home: {
        ru: 16,
        lv: 9
    },
    getPage(page){
        return axios.get(this.url +  '/wp-json/wp/v2/pages/' +page ).then( response =>{
            return response.data.acf;
        })
    },
    getProjects(){
        return axios.get(this.url + '/wp-json/wp/v2/projects/' ).then( response =>{
            return {
                projects: response.data.map(item =>({
                    title: item.acf.project_name,
                    description: item.acf.project_descrription,
                    images: item.acf.projects_images
                }))
            }
        })
    }

}