export const state = () => ({
  locales: ['lv', 'ru'],
  locale: 'lv',
  homePage:[],
  projects:[],
  floors:{
    parkets: [],
    laminats:[],
    vinils:[],
    rieksts:[],
    makls:[]
  }
})

export const mutations = {
  SET_LANG(state, locale) {
    if (state.locales.indexOf(locale) !== -1) {
      state.locale = locale
    }
  },
  setHomepage (state, obj) {
    state.homePage = obj;
  },
  setProjects (state, obj){
    state.projects = obj;
  },
  setFloors(state, obj){
    state.floors[obj[1]] = obj[0];
  }
}
